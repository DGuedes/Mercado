class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.integer :id_product
      t.string :description
      t.float :price
      t.integer :quantity_stock

      t.timestamps
    end
  end
end
